import Phaser from "phaser";
import config from "visual-config-exposer";

class PostGame extends Phaser.Scene {
  constructor() {
    super("endGame");
    this.GAME_WIDTH = 740;
    this.GAME_HEIGHT = 900;
  }

  init(data) {
    console.log("init", data);
  }

  create(data) {
    const background = this.add.image(0, 0, "bg");
    background.setOrigin(0, 0);
    background.setScale(config.preGame.bgImgScaleX, config.preGame.bgImgScaleY);

    const hoverImage = this.add.image(100, 100, "box");
    hoverImage.setVisible(false);
    hoverImage.setScale(0.07);

    const playBtn = this.add.text(
      this.GAME_WIDTH / 2.5,
      this.GAME_HEIGHT / 2,
      "Play Again",
      {
        fontSize: "32px",
        fontFamily: "Helvetica",
        fill: "#000000",
      }
    );

    playBtn.setInteractive();

    playBtn.on("pointerover", () => {
      hoverImage.setVisible(true);
      hoverImage.x = playBtn.x - playBtn.width / 2.5;
      hoverImage.y = playBtn.y + 15;
    });
    playBtn.on("pointerout", () => {
      hoverImage.setVisible(false);
    });
    playBtn.on("pointerdown", () => {
      this.scene.start("playGame");
    });

    const leaderBoardBtn = this.add.text(
      this.GAME_WIDTH / 2.7,
      this.GAME_HEIGHT / 1.7,
      "Leaderboards",
      {
        fontSize: "32px",
        fontFamily: "Helvetica",
        fill: "#000000",
      }
    );

    leaderBoardBtn.setInteractive();

    leaderBoardBtn.on("pointerover", () => {
      hoverImage.setVisible(true);
      hoverImage.x = leaderBoardBtn.x - leaderBoardBtn.width / 2.5;
      hoverImage.y = leaderBoardBtn.y + 15;
    });
    leaderBoardBtn.on("pointerout", () => {
      hoverImage.setVisible(false);
    });
    leaderBoardBtn.on("pointerdown", () => {
      this.scene.start("leaderBoardPost", { score: data.score });
    });

    const ctaBtn = this.add.text(
      this.GAME_WIDTH / 2.7,
      this.GAME_HEIGHT / 1.7,
      `${config.game.ctaText}`,
      {
        fontSize: "32px",
        fontFamily: "Helvetica",
        fill: "#000000",
      }
    );

    ctaBtn.setInteractive();

    ctaBtn.on("pointerover", () => {
      hoverImage.setVisible(true);
      hoverImage.x = ctaBtn.x - ctaBtn.width / 2.5;
      hoverImage.y = ctaBtn.y + 15;
    });
    ctaBtn.on("pointerout", () => {
      hoverImage.setVisible(false);
    });
    ctaBtn.on("pointerdown", this.openExternalLink, this);
  }

  openExternalLink() {
    const url = config.game.ctaUrl;
    const s = window.open(url, "_blank");
    if (s && s.focus) {
      s.focus();
    } else if (!s) {
      window.location.href = url;
    }
  }
}

export default PostGame;
